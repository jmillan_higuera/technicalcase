Case Study SteadyState 2021

Introduction 

The backward-facing step (BFS) model is a good starting point to understand changes in flow properties arising due to separation at the stepping point. An important instance in where this phenomenon can be observed is aircraft aerodynamics, where flow separation and reattachment over an airfoil at different angles of attack is observed. At the same time, as this model has been extensively studied, it provided a good benchmark case when selecting a numerical model suitable to accurately describe the phenomena.

For the case in question, a standard k-e turbulence model was used. Developed by Spalding and Launder in 1974, it minimized the number of unknowns and presents a set of equations that can be applied to a broad range of turbulent applications. This model is the one to be used to run the initial case study and subsequent cases. 


Modeling

An initial case was provided where measurements to the velocity were made at the backward-facing step in the duct floor at 1.03175m from 0m to 0.1143m vertically. A sample of 100 points was used to have enough data points for comparison, using a cellPoint interpolation scheme. The same was done for calculating the reattachment length, albeit a different probing line. An STL model was used to generate the geometry of the backward stepping duct, using snappyMesh to generate the mesh. Boundary conditions consist of walls, inlet, and outlet. The property of the boundary conditions is defined in the 0 folders.  As the geometry had to be enclosed by a background mesh, blockMesh was used to accomplish this task. The mesh was rather coarse, using a (55 11 11) grading, which corresponds to the number of cells in each direction. A time step of 1s was used, while a convergence factor of 1.0e-02 was used (this automatically ends the simulation). As the case is a steady-state case, ddtSchemes is set up as steadyState. 

For the subsequent cases, which improve upon the original case, an increment on the mesh grading was done, increasing it to (75 20 11) to increase the accuracy of the simulation. However, the increment in grading did not capture the flow separation and reattachment phenomena, having to further increase the mesh grading. In this instance, a custom mesh using GMesh was used to increase the grading accordingly. GMesh provides powerful scripting tools alongside its GUI functionality, making it ideal to generate high-fidelity meshes. 

A python script was done to post-process data and generate plots comparing experimental and numerical results. This script should generate a plot comparing the numerical data with an initial simulation setup and an improved mesh. In the case of the reattachment length, the script compares the experimental data with that of the mesh generated using GMesh. 

Results 

As observed in results.png, the initial case setup does not accurately model the results when compared to that of the experimental data. This is due to the poor mesh resolution, which as the cells in the x-direction are calculating large sections of the region where the flow phenomena occur, effectively reducing the resolution. As such, increasing the number of cells in the x-direction increases the accuracy of the results, where the numerical results closely match the experimental results. 

To calculate the reattachment length, an increase in mesh resolution was needed. reattachment.png nicely showcases the velocity vectors in the separation region. A reattachment length ratio of 4.83 was obtained, with an error percentage of 22.7% in comparison to the experimental results. This is within the expected margin of error, as other numerical analyses, albeit with different parameters, obtained a margin of error of 26.74% [1]. 

Recommendations 

Verification and validation of a CFD code are one of the most important aspects when publishing code, as benchmarking numerical models in comparison to experimental data provides a point of reference regarding the fidelity of the numerical method in question in comparison to the experimental data, which at the end is so limited by measurement techniques. This comparison can also help make appropriate decisions regarding if a numerical method is an appropriate match to the data being analyzed or if another model should be considered, and if the CFD case was set up correctly or modifications need to be done on the mesh, time step, residual threshold or relaxation factors. One way to determine the accuracy of the simulation is to do a calculation on the percentage of error, as it was done for the reattachment length. In this particular instance, a percentage of error of 22.7% while within margin, is quite high. To remedy this, realizableKE, LienCubicKE could have been used to model the turbulence. The criteria for convergence could have been decreased to 1E-06, and the time step could have been decreased, however, in this instance as it is a steady-state the time parameter does little except for increasing the effective runtime, which is moot as there is a convergence parameter specified within the simulation.

References

[1] Anugya Singh et al 2020 IOP Conf. Ser.: Mater. Sci. Eng. 912 042060


