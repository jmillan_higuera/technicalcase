// Gmsh project created on Wed May 19 14:07:10 2021
SetFactory("OpenCASCADE");

// points for building the mesh - base
Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 0.1016, 0, 1.0};
Point(3) = {1, 0.1016, 0, 1.0};
Point(4) = {1, 0, 0, 1.0};
Point(5) = {1, -0.0127, 0, 1.0};
Point(6) = {2.0, -0.0127, 0, 1.0};
Point(7) = {2.0, 0.151, 0, 1.0};

Recursive Delete 
{
  Point{7}; 
}

Point(7) = {2.0, 0.1016, 0, 1.0};
Point(8) = {2.0, 0, 0, 1.0};
Point(9) = {1.25, 0, 0, 1.0};
Point(10) = {1.25, -0.0127, 0, 1.0};
Point(11) = {1.25, 0.1016, 0, 1.0};

// lines for building the mesh - derived 
Line(1) = {2, 3};
Line(2) = {4, 3};
Line(3) = {4, 1};
Line(4) = {1, 2};
Line(5) = {5, 10};
Line(6) = {10, 9};
Line(7) = {9, 4};
Line(8) = {4, 5};
Line(9) = {9, 8};
Line(10) = {8, 6};
Line(11) = {6, 10};
Line(12) = {7, 8};
Line(13) = {7, 11};
Line(14) = {11, 9};
Line(15) = {11, 3};

// line loops for defining the plane surface
Line Loop(1) = {1, -2, 3, 4};
Line Loop(2) = {15, -2, -7, -14};
Line Loop(3) = {13, 14, 9, -12};
Line Loop(4) = {5, 6, 7, 8};
Line Loop(5) = {11, 6, 9, 10};

// plane surface 
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};

// transfinite lines so you can refine the mesh
Transfinite Line {1, 3, 4, 2} = 100 Using Progression 1;
Transfinite Line {5} = 250 Using Progression 1;
Transfinite Line {8} = 50 Using Progression 1;
Transfinite Line {12, 14, 13, 9} = 10 Using Progression 1;
Transfinite Line {11, 9, 10} = 25 Using Progression 1;
Transfinite Line {7} = 250 Using Progression 1;
Transfinite Line {15} = 25 Using Progression 1;
Transfinite Line {6} = 50 Using Progression 1;

// transfinite the surface - step needed in order to make the mesh uniform
Transfinite Surface {1};

// recombine surfaces, for making mesh uniform and organizing mesh 
Recombine Surface {1};
Recombine Surface {4};

// extrude
Extrude {0, 0, 0.151} 
{
  Surface{1}; Surface{2}; Surface{3}; Surface{4}; Surface{5};
  	Layers{20};
	Recombine; 
}

// naming of boundaries and internal volume
Physical Volume("internal") = {1, 2, 4, 3, 5};
Physical Surface("walls") = {10, 1, 6, 8, 21, 22, 25, 18, 3, 14, 2, 4, 5, 15, 11, 24, 19, 23};
Physical Surface("outlet") = {17};
Physical Surface("inlet") = {9};


