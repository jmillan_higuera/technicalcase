#built in Spyder 
#created by Jose A. Millan Higuera
#May 20, 2021
import matplotlib.pyplot as plt
import pandas as pd
import os 
import csv

# get adress of current folder (case folder)
currentDir = os.getcwd()

#import data
experimentalDataAddress = currentDir+"/initialCase/experimental.dat"
openFOAMDataAddress_initial = currentDir+"/initialCase/postProcessing/line_1/150/line_U.csv"
openFOAMDataAddress_mesh = currentDir+"/meshImprovement/postProcessing/line_1/150/line_U.csv"
openFOAMDataAddress_reattachment = currentDir+"/reattachmentLength/postProcessing/line_1/150/line_U.csv"

#open data
experimentalData = pd.read_csv(experimentalDataAddress,delimiter="\s+",engine="python")
del experimentalData['V/Ur']
experimentalData.columns = ['Y/H', 'U/Ur', 'V/Ur']
openFoamData_initial = pd.read_csv(openFOAMDataAddress_initial, delimiter=",", engine="python")
openFoamData_mesh = pd.read_csv(openFOAMDataAddress_mesh, delimiter=",", engine="python")
openFoamData_reattachment = pd.read_csv(openFOAMDataAddress_reattachment, delimiter=",", engine="python")

#for experimental data y/h - u/ur
yH = experimentalData['Y/H']
U = experimentalData['U/Ur']
minyH = min(yH)
maxyH = max(yH)
minU = min(U)
maxU = max(U)

#data obtained in the simulation for initial case 
distance_init = openFoamData_initial['distance']
uX_init = openFoamData_initial['U_0']
#data manipulation
uX_ratio_init = uX_init/44.2
distance_ratio_init = distance_init/0.0127

#data obtained in the simulation for mesh cell increment
distance_mesh = openFoamData_mesh['distance']
uX_mesh = openFoamData_mesh['U_0']
#data manipulation
uX_ratio_mesh = uX_mesh/44.2
distance_ratio_mesh = distance_mesh/0.0127

# plotting
plt.plot(yH, U, 'ro', label="experimental")
plt.plot(distance_ratio_init, uX_ratio_init, 'g^', label="simulation")
plt.plot(distance_ratio_mesh, uX_ratio_mesh, 'b+', label = "mesh")
plt.axis([minyH, maxyH, minU, maxU+0.1])
plt.legend(loc="lower right")
plt.xlabel('Y/H')
plt.ylabel("$U/U_r$")
plt.title("Station 8 Results", fontweight="bold")
plt.grid()
plt.savefig('results.png')

plt.show()

# calculate the reattachment lenght 
xrH = 6.26
lenghtdistance = openFoamData_reattachment['distance']
uXlength = openFoamData_reattachment['U_0']
position = 0; 

for i in range(uXlength.size):
    if uXlength[i] > 0:
        position = i
        break
    
lenght_data = [ [0, xrH],
               [0, lenghtdistance[position]/0.0127],
               ["error",  ((xrH-(lenghtdistance[position]/0.0127))/xrH)*100]]

lengthDistanceCSV = pd.DataFrame(lenght_data)
lengthDistanceCSV.columns = ['angle', 'ratio']

lengthDistanceCSV.to_csv('backwardLength.dat', index=False, sep='\t')

        
